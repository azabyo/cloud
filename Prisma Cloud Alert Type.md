# Prisma Cloud Alerts Type

 - Open
    > Prisma Cloud identified a policy violation that triggered the alert and the violation is not yet resolved.
    
    > Prisma Cloud는 경고를 트리거 한 정책 위반을 확인했지만 위반은 아직 해결되지 않습니다.
    
 - Resolved
    > Alerts automatically transition to Resolved state when the issue that caused the policy violation is resolved. An alert can also change to Resolved state due to a change in the policy or alert rule that triggered the alert. A resolved alert can also transition back to the open state if the issue resurfaces or there is a policy or alert rule change that causes the alert to trigger again.

    > 정책 위반의 원인이 된 문제가 해결되면 경고가 자동으로 해결 된 상태로 전환합니다. 경고를 트리거 한 정책 또는 경고 규칙의 변경에 따라 경고가 해결 된 상태로 바뀔 수도 있습니다. 문제가 재발 한 경우 또는 정책 또는 경고 규칙이 변경되어 경고가 다시 트리거 된 경우 해결 된 경고는 오픈 상태로 돌아갈 수 있습니다.

 - Snoozed
   > A Prisma Cloud administrator temporarily dismissed an alert for a specified time period. When the timer expires, the alert automatically changes to an pen or Resolved state depending on whether the issue is fixed.

   > Prisma Cloud 관리자는 지정된 기간 경보를 일시적으로 거부했습니다. 타이머가 만료되면 문제가 수정되었는지 여부에 따라 경보는 자동으로 펜 또는 해결 된 상태로 바뀝니다.

 - Dismissed
   > A Prisma Cloud administrator manually dismissed the alert even though the underlying issue was not resolved. You can manually reopen a dismissed alert if needed.

   > Prisma Cloud 관리자는 근본적인 문제가 해결되지 않아도 수동으로 경보를 기각했습니다. 필요에 따라 기각 된 경고를 수동으로 다시 열 수 있습니다.